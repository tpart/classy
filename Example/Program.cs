﻿using classy.Network;
using classy.Packets;
using classy.Packets.Client;
using classy.Packets.Server;


class Program
{
    private static CancellationToken _cancellationToken = new CancellationToken();

    public static void Main(string[] args) {
        Console.WriteLine("Started Minecraft Client.");

        MinecraftConnection connection = new MinecraftConnection("127.0.0.1", 25565, _cancellationToken, ReceivedPacket);
        
        IPacket loginPacket = new PlayerIdentificationPacket {
            ProtocolVersion = 0x07,
            Username = "Steve",
            VerificationKey = "anything",
        };
        
        connection.SendPacket(loginPacket);
    }

    public static void ReceivedPacket(IPacket packet)
    {
        if (packet is ServerIdentificationPacket serverIdentificationPacket)
        {
            Console.WriteLine($"ServerName: {serverIdentificationPacket.ServerName}");
        }
        else
        {
            Console.WriteLine($"Received unhandled packet ({packet.GetType()})");
        }
    }
}