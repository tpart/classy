using System;
using System.IO.Compression;
using System.Numerics;


namespace classy.Data
{
    public class Map
    {
        public short XSize;
        public short YSize;
        public short ZSize;
        public int Volume;
        public byte[]? chunksDataCompressed = null;
        public byte[]? chunksData = null;
        public byte[]? blocks = null;

        public Map() { }

        public void AddChunkData(byte[] chunkData)
        {
            if (chunksDataCompressed == null)
            {
                chunksDataCompressed = chunkData;
            }
            else
            {
                // Add new chunkData to chunksDataCompressed Array
                byte[] combined = new byte[chunksDataCompressed.Length + chunkData.Length];
                Buffer.BlockCopy(chunksDataCompressed, 0, combined, 0, chunksDataCompressed.Length);
                Buffer.BlockCopy(chunkData, 0, combined, chunksDataCompressed.Length, chunkData.Length);
                chunksDataCompressed = combined;
            }
        }

        public void ProcessLevel(short XSize, short YSize, short ZSize)
        {
            this.XSize = XSize;
            this.YSize = YSize;
            this.ZSize = ZSize;
            Volume = XSize * YSize * ZSize;

            if (chunksDataCompressed != null)
            {
                // Decompress chunk data
                try
                {
                    using (MemoryStream decompressedChunkStream = new MemoryStream())
                    {
                        using (Stream compressedChunkStream = new MemoryStream(chunksDataCompressed))
                        {
                            using (GZipStream gZipStream = new GZipStream(compressedChunkStream, CompressionMode.Decompress))
                            {
                                gZipStream.CopyTo(decompressedChunkStream);
                            }
                        }
                        chunksData = decompressedChunkStream.ToArray();
                    }
                }
                catch
                {
                    Console.WriteLine($"Server did not send valid gzip! Skipping extraction.");
                    chunksData = chunksDataCompressed;
                }
            }

            if (chunksData != null)
            {
                // Process data
                try
                {
                    byte[] blockCountBytes = new byte[4];
                    Buffer.BlockCopy(chunksData, 0, blockCountBytes, 0, 4);
                    int blockCount = BitConverter.ToInt32(blockCountBytes.Reverse().ToArray());
                    byte[] blockData = chunksData.Skip(4).ToArray();
                    if (blockData.Length != blockCount)
                    {
                        Console.WriteLine($"Error processing chunks: Block count does not match byte array length.");
                        return;
                    }
                    blocks = blockData;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error processing chunks: {e.ToString()}");
                }
            }
        }

        public void ProcessLevel(short XSize, short YSize, short ZSize, byte[] chunksDataCompressed)
        {
            this.chunksDataCompressed = chunksDataCompressed;
            ProcessLevel(XSize, YSize, ZSize);
        }

        public Block? GetBlock(short x, short y, short z)
        {
            if (blocks == null)
            {
                return null;
            }
            int idx = x + XSize * (z + ZSize * y);
            if (idx < blocks.Length && idx >= 0)
                return (Block)blocks[idx];
            else
                return null;
        }

        public void SetBlock(short x, short y, short z, Block block)
        {
            if (blocks == null)
            {
                return;
            }
            int idx = x + XSize * (z + ZSize * y);
            if (idx < blocks.Length && idx >= 0)
                blocks[idx] = (byte)block;
        }

        public Block? GetBlock(Vector3 position)
        {
            short x = (short)position.X;
            short y = (short)position.Y;
            short z = (short)position.Z;
            return GetBlock(x, y, z);
        }
    }
}