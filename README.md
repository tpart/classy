# classy
A complete Minecraft Classic networking library written in C#

## Usage
Add the [nuget package](https://www.nuget.org/packages/classy):
```
dotnet add package classy
```

Here's a small example of how to connect to a Minecraft Classic server:

```C#
using classy.Network;
using classy.Packets;
using classy.Packets.Client;
using classy.Packets.Server;

class Program
{
    private static CancellationToken _cancellationToken = new CancellationToken();

    public static void Main(string[] args) {
        MinecraftConnection connection = new MinecraftConnection("127.0.0.1", 25565, _cancellationToken, ReceivedPacket);
                
        IPacket loginPacket = new PlayerIdentificationPacket {
            ProtocolVersion = 0x07,
            Username = "Steve",
            VerificationKey = "anything",
        };
                
        connection.SendPacket(loginPacket);
    }

    public static void ReceivedPacket(IPacket packet)
    {
        if (packet is ServerIdentificationPacket serverIdentificationPacket)
        {
            Console.WriteLine($"ServerName: {serverIdentificationPacket.ServerName}");
        }
        else
        {
            Console.WriteLine($"Received unhandled packet (type: {packet.GetType()})");
        } 
    }
}
```

For more information about Minecraft Classic networking, please visit [wiki.vg](https://wiki.vg/Classic_Protocol).