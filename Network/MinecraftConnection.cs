using classy.Packets;
using System.Text;
using System.Net.Sockets;


namespace classy.Network
{
    public class MinecraftConnection
    {
        public MinecraftStream minecraftStream;
        private CancellationToken cancellationToken { get; }
        private PacketRegistry packetRegistry = new PacketRegistry();
        public Thread NetworkThread { get; private set; }
        private ReceivedPacketCallBack callBack;

        public MinecraftConnection(string serverUrl, int port, CancellationToken cancellationToken, ReceivedPacketCallBack callBack)
        {
            minecraftStream = new MinecraftStream(serverUrl, port);
            this.cancellationToken = cancellationToken;
            this.callBack = callBack;
            NetworkThread = new Thread(ProcessNetwork);
            NetworkThread.Name = "classy - Network Thread";
            NetworkThread.Start();
        }

        public delegate void ReceivedPacketCallBack(IPacket packet);

        public void ProcessNetwork()
        {
            try
            {
                SpinWait sw = new SpinWait();

                while (!cancellationToken.IsCancellationRequested)
                {
                    if (cancellationToken.IsCancellationRequested)
                        break;

                    if (minecraftStream.IsDataAvailable())
                    {
                        IPacket? packet = TryReadPacket(minecraftStream);
                        if (packet != null)
                        {
                            callBack(packet);
                        }
                    }
                    else
                    {
                        sw.SpinOnce();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error reading packet: {e.Message}");
            }
        }

        public IPacket? TryReadPacket(MinecraftStream minecraftStream)
        {
            byte packetId = minecraftStream.ReadByte();
            IPacket? packet = packetRegistry.GetClientPacket(packetId);
            if (packet != null)
            {
                packet.Decode(minecraftStream);
                return packet;
            }
            else
            {
                Console.WriteLine($"Received packet that has not yet been implemented. ID: {packetId}");
                return null;
            }
        }

        public void SendPacket(IPacket packet)
        {
            byte? packetId = packetRegistry.GetServerId(packet);
            if (packetId != null)
            {
                minecraftStream.WriteByte((byte)packetId);
                packet.Encode(minecraftStream);
                minecraftStream.ClearBuffer();
            }
            else
            {
                Console.WriteLine($"Could not send packet because it has not yet been implemented.");
            }
        }
    }
}