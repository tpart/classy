﻿using System.Text;
using System.Net.Sockets;


namespace classy.Network
{
    public class MinecraftStream
    {
        public NetworkStream stream;
        public TcpClient client;
        private byte[] buffer = new byte[0];

        public MinecraftStream(string serverUrl, int port)
        {
            client = new TcpClient(serverUrl, port);
            stream = client.GetStream();
        }

        public byte ReadByte()
        {
            return (byte)stream.ReadByte();
        }

        public sbyte ReadSByte()
        {
            return (sbyte)stream.ReadByte();
        }

        public byte[] ReadByteArray()
        {
            return ReadBytes(1024);
        }

        public short ReadShort()
        {
            byte[] shortBytes = ReadBytes(2);
            Array.Reverse(shortBytes);
            return BitConverter.ToInt16(shortBytes, 0);
        }

        public int ReadInt()
        {
            byte[] intBytes = ReadBytes(4);
            Array.Reverse(intBytes);
            return BitConverter.ToInt32(intBytes, 0);
        }

        public string ReadString()
        {
            byte[] stringBytes = ReadBytes(64);
            return Encoding.ASCII.GetString(stringBytes).TrimEnd(' ');
        }

        public void WriteByte(byte data)
        {
            byte[] temp = new byte[buffer.Length + 1];
            Buffer.BlockCopy(buffer, 0, temp, 0, buffer.Length);
            temp[buffer.Length] = data;
            buffer = temp;
        }

        public void WriteSByte(sbyte data)
        {
            byte[] temp = new byte[buffer.Length + 1];
            Buffer.BlockCopy(buffer, 0, temp, 0, buffer.Length);
            temp[buffer.Length] = (byte)data;
            buffer = temp;
        }

        public void WriteByteArray(byte[] Send)
        {
            WriteBytes(Send);
        }

        public void WriteShort(short data)
        {
            byte[] shortBytes = BitConverter.GetBytes(data);
            Array.Reverse(shortBytes);
            WriteBytes(shortBytes);
        }

        public void WriteInt(int data)
        {
            byte[] intBytes = BitConverter.GetBytes(data);
            Array.Reverse(intBytes);
            WriteBytes(intBytes);
        }

        public void WriteString(string data)
        {
            data = data.PadRight(64, ' ');
            WriteBytes(Encoding.ASCII.GetBytes(data));
        }

        public bool IsDataAvailable()
        {
            return stream.DataAvailable;
        }

        private byte[] ReadBytes(int size)
        {
            var data = new byte[size];
            int bytesRead = stream.Read(data, 0, size);

            if (bytesRead == size)
            {
                return data;
            }

            while (true)
            {
                if (bytesRead != size)
                {
                    int newSize = size - bytesRead;

                    bytesRead = stream.Read(data, bytesRead, newSize);

                    if (bytesRead != newSize)
                    {
                        size = newSize;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return data;
        }

        private void WriteBytes(byte[] bytes)
        {
            if (buffer == null)
            {
                buffer = bytes;
            }
            else
            {
                int tempLength = buffer.Length + bytes.Length;
                byte[] temp = new byte[tempLength];

                Buffer.BlockCopy(buffer, 0, temp, 0, buffer.Length);
                Buffer.BlockCopy(bytes, 0, temp, buffer.Length, bytes.Length);

                buffer = temp;
            }
        }

        public void ClearBuffer()
        {
            try
            {
                stream.Write(buffer, 0, buffer.Length);
                buffer = new byte[0];
            }
            catch
            {
                Console.WriteLine("Failed to send buffer!");
            }
        }
    }
}