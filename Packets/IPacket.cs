using classy.Network;

namespace classy.Packets
{
    public interface IPacket
    {
        public void Encode(MinecraftStream minecraftStream);
        public void Decode(MinecraftStream minecraftStream);
    }
}
