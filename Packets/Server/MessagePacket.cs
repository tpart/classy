using classy.Network;


namespace classy.Packets.Server
{
    public class MessagePacket : IPacket
    {
        public byte Unused = 0xFF;
        public string Message { get; set; } = "";
        

        public void Decode(MinecraftStream minecraftStream)
        {
            Unused = minecraftStream.ReadByte();
            Message = minecraftStream.ReadString();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteByte(Unused);
            minecraftStream.WriteString(Message);
        }
    }
}