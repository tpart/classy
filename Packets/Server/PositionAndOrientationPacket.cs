using classy.Network;


namespace classy.Packets.Server
{
    public class PositionAndOrientationPacket : IPacket
    {
        public byte PlayerId { get; set; }
        public short X { get; set; }
        public short Y { get; set; }
        public short Z { get; set; }
        public byte Yaw { get; set; }
        public byte Pitch { get; set; }

        public void Decode(MinecraftStream minecraftStream)
        {
            PlayerId = minecraftStream.ReadByte();
            X = minecraftStream.ReadShort();
            Y = minecraftStream.ReadShort();
            Z = minecraftStream.ReadShort();
            Yaw = minecraftStream.ReadByte();
            Pitch = minecraftStream.ReadByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteByte(PlayerId);
            minecraftStream.WriteShort(X);
            minecraftStream.WriteShort(Y);
            minecraftStream.WriteShort(Z);
            minecraftStream.WriteByte(Yaw);
            minecraftStream.WriteByte(Pitch);
        }
    }
}