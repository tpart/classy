using classy.Network;


namespace classy.Packets.Client
{
    public class UpdateUserTypePacket : IPacket
    {
        public byte UserType { get; set; }
        

        public void Decode(MinecraftStream minecraftStream)
        {
            UserType = minecraftStream.ReadByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteByte(UserType);
        }
    }
}