using classy.Network;


namespace classy.Packets.Client
{
    public class PositionAndOrientationPacket : IPacket
    {
        public sbyte PlayerId { get; set; }
        public short X { get; set; }
        public short Y { get; set; }
        public short Z { get; set; }
        public byte Yaw { get; set; }
        public byte Pitch { get; set; }

        public void Decode(MinecraftStream minecraftStream)
        {
            PlayerId = minecraftStream.ReadSByte();
            X = minecraftStream.ReadShort();
            Y = minecraftStream.ReadShort();
            Z = minecraftStream.ReadShort();
            Yaw = minecraftStream.ReadByte();
            Pitch = minecraftStream.ReadByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteSByte(PlayerId);
            minecraftStream.WriteShort(X);
            minecraftStream.WriteShort(Y);
            minecraftStream.WriteShort(Z);
            minecraftStream.WriteByte(Yaw);
            minecraftStream.WriteByte(Pitch);
        }
    }
}