using classy.Network;


namespace classy.Packets.Client
{
    public class LevelDataChunkPacket : IPacket
    {
        public short ChunkLength { get; set; }
        public byte[] ChunkData { get; set; } = new byte[1024];
        public byte PercentComplete { get; set; }

        public void Decode(MinecraftStream minecraftStream)
        {
            ChunkLength = minecraftStream.ReadShort();
            ChunkData = minecraftStream.ReadByteArray();
            if (ChunkData.Length != ChunkLength)
            {
                byte[] newChunkData = new byte[ChunkLength];
                Buffer.BlockCopy(ChunkData, 0, newChunkData, 0, ChunkLength);
                ChunkData = newChunkData;
            }
            PercentComplete = minecraftStream.ReadByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteShort(ChunkLength);
            minecraftStream.WriteByteArray(ChunkData);
            minecraftStream.WriteByte(PercentComplete);
        }
    }
}