using classy.Network;


namespace classy.Packets.Client
{
    public class ServerIdentificationPacket : IPacket
    {
        public byte ProtocolVersion { get; set; }
        public string ServerName { get; set; } = "";
        public string ServerMotd { get; set; } = "";
        public byte UserType { get; set; }

        public void Decode(MinecraftStream minecraftStream)
        {
            ProtocolVersion = minecraftStream.ReadByte();
            ServerName = minecraftStream.ReadString();
            ServerMotd = minecraftStream.ReadString();
            UserType = minecraftStream.ReadByte();
        }

        public void Encode(MinecraftStream minecraftStream)
        {
            minecraftStream.WriteByte(ProtocolVersion);
            minecraftStream.WriteString(ServerName);
            minecraftStream.WriteString(ServerMotd);
            minecraftStream.WriteByte(UserType);
        }
    }
}